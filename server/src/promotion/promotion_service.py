class PromotionService:
    def __init__(self, promotion_repository):
        self.promotion_repository = promotion_repository

    def get_promotions(self):
        return self.promotion_repository.get_all_promotions()
