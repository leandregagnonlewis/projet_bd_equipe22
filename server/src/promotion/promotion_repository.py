class PromotionRepository:

    def __init__(self, database_connector):
        self.database_connector = database_connector

    def get_all_promotions(self):
        query = "SELECT * FROM promotion"
        promotions = self.database_connector.make_select_call(query)
        return promotions
