from flask import jsonify


class PromotionResource:
    def __init__(self, promotion_service):
        self.promotion_service = promotion_service

    def get_promotions(self, request):
        promotions = self.promotion_service.get_promotions()
        return jsonify({"promotions": promotions})

