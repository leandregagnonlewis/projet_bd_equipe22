import os
import time

from mysql.connector import connect
from mysql.connector import Error


class DatabaseConnector:
    def __init__(self):
        if os.environ.get('DEVENV') == 'prod':
            self.host = "bd"
            self.port = "3306"
            self.username = "root"
            self.password = "root"
        else:
            self.host = "localhost"
            self.port = "1337"
            self.username = "root"
            self.password = None
        self.database_name = "fair_beans"
        self.active_promotion_on_product = dict()

    def make_select_call(self, query):
        database_connector = connect(host=self.host, port=self.port, user=self.username, password=self.password,
                                     database=self.database_name)
        cursor = database_connector.cursor()
        cursor.execute(query)
        tuples = self.wrap_sql_cursor_value(cursor)
        database_connector.close()
        return tuples

    def make_product_select_call(self, query):
        database_connector = connect(host=self.host, port=self.port, user=self.username, password=self.password,
                                     database=self.database_name)
        cursor = database_connector.cursor()
        cursor.execute(query)
        tuples = self.wrap_sql_cursor_value_with_promotion(cursor)
        database_connector.close()
        return tuples

    def make_single_item_select(self, query):
        database_connector = connect(host=self.host, port=self.port, user=self.username, password=self.password,
                                     database=self.database_name)
        cursor = database_connector.cursor()
        cursor.execute(query)
        value = self.wrap_unique_sql_cursor_value(cursor)
        database_connector.close()
        return value

    def make_update_table_call(self, query):
        database_connector = connect(host=self.host, port=self.port, user=self.username, password=self.password,
                                     database=self.database_name)
        cursor = database_connector.cursor()
        cursor.execute(query)
        database_connector.close()

    def make_insert_call(self, query):
        database_connector = connect(host=self.host, port=self.port, user=self.username, password=self.password,
                                     database=self.database_name)
        cursor = database_connector.cursor()
        cursor.execute(query)
        database_connector.commit()
        database_connector.close()

    def wait_for_connection(self):
        connection = False
        while not connection:
            try:
                database_connector = connect(host=self.host, port=self.port, user=self.username, password=self.password,
                                             database=self.database_name)
                if database_connector.is_connected():
                    connection = True

            except Error as e:
                time.sleep(1)

    @staticmethod
    def wrap_sql_cursor_value(cursor):
        column = cursor.column_names
        wrapped_cursor_values = []
        for cursor_value in cursor:
            wrapped_value = {column: value for column, value in zip(column, cursor_value)}
            wrapped_cursor_values.append(wrapped_value)
        return wrapped_cursor_values

    def wrap_sql_cursor_value_with_promotion(self, cursor):
        column = cursor.column_names
        wrapped_cursor_values = []
        for cursor_value in cursor:
            wrapped_value = {column: value for column, value in zip(column, cursor_value)}
            if wrapped_value['id'] in self.active_promotion_on_product:
                query = "SELECT * FROM promotion WHERE id = '{}'"\
                    .format(self.active_promotion_on_product[wrapped_value['id']][0])
                wrapped_value['promotion'] = self.make_single_item_select(query)
            else:
                wrapped_value['promotion'] = None
            wrapped_cursor_values.append(wrapped_value)
        return wrapped_cursor_values

    @staticmethod
    def wrap_unique_sql_cursor_value(cursor):
        column = cursor.column_names
        value = cursor.fetchone()
        if value is not None:
            wrapped_value = {column: value for column, value in zip(column, value)}
            return wrapped_value
        return None

