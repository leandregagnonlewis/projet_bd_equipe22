def filter_by_roast(products, roasts):
    filtered_products = []
    roast_list = roasts.split(',')
    for product in products:
        for roast in roast_list:
            if product['roast'] == roast:
                filtered_products.append(product)
                break
    return filtered_products


def filter_by_roaster(products, roasters):
    filtered_products = []
    roaster_list = roasters.split(',')
    for product in products:
        for roaster in roaster_list:
            if product['roaster'] == roaster:
                filtered_products.append(product)
                break
    return filtered_products


def get_number_of_pages(max_number_of_products_per_page, total_number_of_products):
    if total_number_of_products % max_number_of_products_per_page == 0:
        return max(1, total_number_of_products // max_number_of_products_per_page)
    return total_number_of_products // max_number_of_products_per_page + 1


def get_first_and_last_products_index(page, max_number_of_product_per_page, total_number_of_products):
    first_product_index = (page - 1) * max_number_of_product_per_page + 1
    last_product_index = min(page * max_number_of_product_per_page, total_number_of_products)
    return first_product_index, last_product_index


def reduce_products_to_fit_in_page(products, index):
    # to convert from index starting from one to index starting from zero
    return products[index[0] - 1: index[1]]


def sort(products, sort_type):
    options = {
        'Name: A - Z': sort_alphabetically(products, False),
        'Name: Z - A': sort_alphabetically(products, True),
        'Price: Low to High': sort_by_price(products, False),
        'Price: High to Low': sort_by_price(products, True)
    }
    return options[sort_type]


def sort_alphabetically(products, reverse):
    return sorted(products, key=lambda product: product['name'], reverse=reverse)


def sort_by_price(products, reverse):
    return sorted(products, key=lambda product: product['price'], reverse=reverse)
