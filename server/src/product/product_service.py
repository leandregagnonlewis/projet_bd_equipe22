class ProductService:
    def __init__(self, product_repository):
        self.product_repository = product_repository
        self.current_category = None
        self.current_search = None
        self.current_promotion = None
        self.product_cache = None

    def get_products(self):
        self.product_cache = self.product_repository.get_all_products()
        self.current_search = None
        self.current_category = None
        self.current_promotion = None
        return self.product_cache

    def get_products_by_category(self, category):
        if self.current_category != category:
            self.current_category = category
            self.current_search = None
            self.current_promotion = None
            if category == 'Promotions':
                self.product_cache = self.product_repository.get_all_products_in_promotion()
            else:
                self.product_cache = self.product_repository.get_products_by_category(category)
        return self.product_cache

    def get_products_by_search(self, search):
        if self.current_search != search:
            self.current_search = search
            self.current_category = None
            self.current_promotion = None
            self.product_cache = self.product_repository.get_products_by_search(search)
        return self.product_cache

    def get_products_by_promotion(self, promotion):
        if self.current_promotion != promotion:
            self.current_search = None
            self.current_category = None
            self.current_promotion = promotion
            self.product_cache = self.product_repository.get_products_by_promotion(promotion)
        return self.product_cache

    def get_roasts(self):
        roasts = []
        for product in self.product_cache:
            roast_in_list = False
            for roast in roasts:
                if product['roast'] == roast:
                    roast_in_list = True
                    break
            if not roast_in_list:
                roasts.append(product['roast'])
        return roasts

    def get_roasters(self):
        roasters = []
        for product in self.product_cache:
            roaster_in_list = False
            for roaster in roasters:
                if product['roaster'] == roaster:
                    roaster_in_list = True
                    break
            if not roaster_in_list:
                roasters.append(product['roaster'])
        return roasters




