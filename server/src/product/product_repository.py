class ProductRepository:

    def __init__(self, database_connector):
        self.product_in_promotion = dict()
        self.active_promotion_on_product = dict()
        self.database_connector = database_connector
        self.database_connector.wait_for_connection()
        self.initialize_product_in_promotion()

    def get_all_products(self):
        query = "SELECT * FROM product"
        products = self.database_connector.make_product_select_call(query)
        return products

    def get_products_by_category(self, category):
        query = "SELECT * FROM product WHERE category = '{}'".format(category)
        products = self.database_connector.make_product_select_call(query)
        return products

    def get_products_by_search(self, search):
        query = "SELECT * FROM product WHERE name LIKE '%{}%'".format(search)
        products = self.database_connector.make_product_select_call(query)
        return products

    def get_products_by_promotion(self, promotion):
        query = "SELECT * FROM product WHERE id IN {}".format(self.__format_list_for_mysql(
            self.product_in_promotion[int(promotion)]))
        products = self.database_connector.make_product_select_call(query)
        return products

    def get_all_products_in_promotion(self):
        ids = list(self.active_promotion_on_product.keys())
        query = "SELECT * FROM product WHERE id IN {}".format(self.__format_list_for_mysql(ids))
        products = self.database_connector.make_product_select_call(query)
        return products

    def initialize_product_in_promotion(self):
        product_query = "SELECT * FROM product"
        products = self.database_connector.make_select_call(product_query)
        promotion_query = "SELECT * FROM promotion"
        promotions = self.database_connector.make_select_call(promotion_query)
        for promotion in promotions:
            for product in products:
                attribute = promotion['attribute']
                if product[attribute] == promotion['selector']:
                    if promotion['id'] not in self.product_in_promotion:
                        self.product_in_promotion[promotion['id']] = [product['id']]
                    else:
                        self.product_in_promotion[promotion['id']].append(product['id'])
                    if product['id'] not in self.active_promotion_on_product \
                            or promotion['discount'] > self.active_promotion_on_product[product['id']][1]:
                        self.active_promotion_on_product[product['id']] = (promotion['id'], promotion['discount'])
        self.database_connector.active_promotion_on_product = self.active_promotion_on_product

    @staticmethod
    def __format_list_for_mysql(a_list):
        a_string = '({}'.format(a_list[0])
        for i in range(1, len(a_list)):
            a_string += ',{}'.format(a_list[i])
        a_string += ')'
        return a_string
