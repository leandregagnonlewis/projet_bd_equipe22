from flask import jsonify
import src.product.product_filter_utils as product_filter


class ProductResource:
    def __init__(self, product_service):
        self.product_service = product_service

    def get_products_page(self, request, page, max_number_of_products_per_page):
        page = int(page)
        max_number_of_products_per_page = int(max_number_of_products_per_page)
        arguments = request.args
        if arguments.get('category'):
            products = self.product_service.get_products_by_category(arguments.get('category'))
        elif arguments.get('search'):
            products = self.product_service.get_products_by_search(arguments.get('search'))
        elif arguments.get('promotion'):
            products = self.product_service.get_products_by_promotion(arguments.get('promotion'))
        else:
            products = self.product_service.get_products()
        if arguments.get('roasts'):
            products = product_filter.filter_by_roast(products, arguments.get('roasts'))
        if arguments.get('roasters'):
            products = product_filter.filter_by_roaster(products, arguments.get('roasters'))
        if arguments.get('sort'):
            products = product_filter.sort(products, arguments.get('sort'))
        roasts = self.product_service.get_roasts()
        roasters = self.product_service.get_roasters()
        total_number_of_products = len(products)
        number_of_pages = product_filter.get_number_of_pages(max_number_of_products_per_page, total_number_of_products)
        first_and_last_products_index = product_filter\
            .get_first_and_last_products_index(page, max_number_of_products_per_page, total_number_of_products)
        products = product_filter.reduce_products_to_fit_in_page(products, first_and_last_products_index)
        return jsonify({"products": products,
                        "roasts": roasts,
                        "roasters": roasters,
                        "numberOfPages": number_of_pages,
                        "totalNumberOfProducts": total_number_of_products,
                        "firstProductIndex": first_and_last_products_index[0],
                        "lastProductIndex": first_and_last_products_index[1]})
