class AccountRepository:

    def __init__(self, database_connector):
        self.database_connector = database_connector

    def create_account(self, email, password):
        query = "INSERT INTO user(email, password) VALUE ('{}','{}')".format(email, password)
        self.database_connector.make_insert_call(query)

    def get_account_by_email(self, email):
        query = "SELECT * FROM user WHERE email = '{}'".format(email)
        account = self.database_connector.make_select_call(query)
        return account

    def create_session(self, token, email):
        query = "INSERT INTO session(token, email) VALUE ('{}','{}')".format(token, email)
        self.database_connector.make_insert_call(query)

    def delete_session(self, token):
        query = "DELETE FROM session WHERE token = '{}'".format(token)
        self.database_connector.make_insert_call(query)

    def get_session_by_token(self, token):
        query = "SELECT * FROM session WHERE token = '{}'".format(token)
        session = self.database_connector.make_select_call(query)
        return session

    def insert_into_cart(self, email, product_id, quantity):
        query = "INSERT INTO cart(email, product_id, quantity) VALUE ('{}','{}','{}')"\
            .format(email, product_id, quantity)
        self.database_connector.make_insert_call(query)

    def get_all_products_in_cart(self, email):
        query = "select id, name, category, origin, species, roast, roaster, price, flavours, imageUrl, quantity " \
                "from cart inner join product on cart.product_id = product.id WHERE email = '{}'".format(email)
        products = self.database_connector.make_product_select_call(query)
        return products

    def update_product_quantity_in_cart(self, email, product_id, quantity):
        query = "UPDATE cart SET quantity = '{}' WHERE email='{}' AND product_id = {}"\
            .format(quantity, email, product_id)
        self.database_connector.make_insert_call(query)

    def delete_product_from_cart(self, email, product_id):
        query = "DELETE FROM cart WHERE email='{}' AND product_id = '{}'"\
            .format(email, product_id)
        self.database_connector.make_insert_call(query)

    def get_product_in_cart(self, email, product_id):
        query = "select id, name, category, origin, species, roast, roaster, price, flavours, imageUrl, quantity " \
                "from cart inner join product on cart.product_id = product.id WHERE email='{}' AND product_id = {}" \
            .format(email, product_id)
        products = self.database_connector.make_product_select_call(query)[0]
        return products

    def insert_into_command(self, date, email, address):
        query = "INSERT INTO command(id, command_date, email, first_name, last_name, street, city, zipcode, country)" \
                " VALUE (0,'{}','{}','{}','{}','{}','{}','{}','{}')"\
            .format(date, email, address['firstName'], address['lastName'], address['street'],
                    address['city'], address['zipcode'], address['country'])
        self.database_connector.make_insert_call(query)

    def get_last_command_by_email(self, email):
        query = "SELECT * FROM command WHERE email = '{}' order by id DESC".format(email)
        command = self.database_connector.make_single_item_select(query)
        return command

    def get_all_commands_by_email(self, email):
        query = "SELECT * FROM command WHERE email = '{}'".format(email)
        commands = self.database_connector.make_select_call(query)
        return commands

    def get_product_in_command_by_command_id(self, command_id):
        query = "select id, name, category, origin, species, roast, roaster, price, flavours, imageUrl, quantity " \
                "from product_in_command inner join product on product_in_command.product_id = product.id " \
                "WHERE command_id = '{}'".format(command_id)
        products = self.database_connector.make_product_select_call(query)
        return products
