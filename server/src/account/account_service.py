import binascii
import hashlib
import os
from time import sleep


class AccountService:
    def __init__(self, account_repository):
        self.account_repository = account_repository

    def get_account_by_email(self, email):
        return self.account_repository.get_account_by_email(email)

    def check_if_account_exists(self, email):
        if len(self.account_repository.get_account_by_email(email)) == 0:
            return False
        return True

    def create_account(self, email, password):
        self.account_repository.create_account(email, self.__hash_password(password))

    def create_session(self, token, email):
        self.account_repository.create_session(token, email)

    def delete_session(self, token):
        self.account_repository.delete_session(token)

    def validate_credentials(self, email, password):
        account = self.account_repository.get_account_by_email(email)
        if len(account) == 0:
            return False
        return self.__compare_password(account[0]['password'], password)

    def get_email_by_token(self, token):
        return self.account_repository.get_session_by_token(token)[0]['email']

    def insert_into_cart(self, email, product_id, quantity):
        self.account_repository.insert_into_cart(email, product_id, quantity)

    def get_all_products_in_cart(self, email):
        return self.account_repository.get_all_products_in_cart(email)

    def update_product_in_cart(self, email, product_id, quantity):
        self.account_repository.update_product_quantity_in_cart(email, product_id, quantity)
        return self.account_repository.get_product_in_cart(email, product_id)

    def delete_product_from_cart(self, email, product_id):
        self.account_repository.delete_product_from_cart(email, product_id)

    def get_product_in_cart(self, email, product_id):
        return self.account_repository.get_product_in_cart(email, product_id)

    def insert_into_command(self, date, email, address):
        self.account_repository.insert_into_command(date, email, address)

    def get_last_command_by_email(self, email):
        return self.account_repository.get_last_command_by_email(email)

    def get_all_commands_by_email(self, email):
        return self.account_repository.get_all_commands_by_email(email)

    def get_product_in_command_by_command_id(self, command_id):
        return self.account_repository.get_product_in_command_by_command_id(command_id)

    @staticmethod
    def __hash_password(password):
        salt = hashlib.sha256(os.urandom(60)).hexdigest().encode('ascii')
        hashed = hashlib.pbkdf2_hmac('sha512', password.encode('utf-8'), salt, 100000)
        hashed = binascii.hexlify(hashed)
        return (salt + hashed).decode('ascii')

    @staticmethod
    def __compare_password(stored_password, provided_password):
        salt = stored_password[:64]
        stored_password = stored_password[64:]
        hashed = hashlib.pbkdf2_hmac('sha512', provided_password.encode('utf-8'), salt.encode('ascii'), 100000)
        hashed = binascii.hexlify(hashed).decode('ascii')
        return hashed == stored_password
