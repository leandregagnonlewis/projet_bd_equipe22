from uuid import uuid4
from flask import jsonify
import datetime


class AccountResource:
    def __init__(self, account_service):
        self.account_service = account_service

    def validate_account_creation(self, form):
        return not self.account_service.check_if_account_exists(form.get('email'))

    def create_account(self, form):
        email = form.get('email')
        password = form.get('password')
        self.account_service.create_account(email, password)
        token = uuid4()
        self.account_service.create_session(token, email)
        return token

    def sign_out(self, token):
        self.account_service.delete_session(token)

    def log_in(self, form):
        email = form.get('email')
        password = form.get('password')
        if self.account_service.validate_credentials(email, password):
            token = uuid4()
            self.account_service.create_session(token, email)
            return jsonify({'token': token})
        else:
            return jsonify({'error': 'This password doesn\'t match the email'})

    def add_to_cart(self, form):
        token = form.get('token')
        product_id = form.get('productId')
        quantity = form.get('quantity')
        email = self.account_service.get_email_by_token(token)
        self.account_service.insert_into_cart(email, product_id, quantity)
        return jsonify({'confirmation': 'product successfully added to cart'})

    def get_all_products_in_cart(self, token):
        email = self.account_service.get_email_by_token(token)
        products = self.account_service.get_all_products_in_cart(email)
        return jsonify({"products": products})

    def update_product_in_cart(self, form):
        token = form.get('token')
        product_id = form.get('productId')
        quantity = form.get('quantity')
        email = self.account_service.get_email_by_token(token)
        updated_product = self.account_service.update_product_in_cart(email, product_id, quantity)
        return jsonify({"updatedProduct": updated_product})

    def delete_product_from_cart(self, form):
        token = form.get('token')
        product_id = form.get('productId')
        email = self.account_service.get_email_by_token(token)
        self.account_service.delete_product_from_cart(email, product_id)
        return jsonify({'confirmation': 'product successfully deleted from cart'})

    def get_product_in_cart(self, token, product_id):
        email = self.account_service.get_email_by_token(token)
        product = self.account_service.get_product_in_cart(email, product_id)
        return jsonify({"product": product})

    def add_order(self, form):
        token = form.get('token')
        address = form.get('address')
        email = self.account_service.get_email_by_token(token)
        date = (datetime.date.today()).isoformat()
        self.account_service.insert_into_command(date, email, address)
        return jsonify({'confirmation': 'product successfully added to cart'})

    def get_last_command(self, token):
        email = self.account_service.get_email_by_token(token)
        command = self.account_service.get_last_command_by_email(email)
        return jsonify({'order': command})

    def get_all_commands(self, token):
        email = self.account_service.get_email_by_token(token)
        commands = self.account_service.get_all_commands_by_email(email)
        return jsonify({'orders': commands})

    def get_product_in_command_by_command_id(self, command_id):
        products = self.account_service.get_product_in_command_by_command_id(command_id)
        return jsonify({'products': products})
