import os

from flask import Flask, jsonify
from flask_cors import CORS
from flask import request

from src.account.account_repository import AccountRepository
from src.account.account_resource import AccountResource
from src.account.account_service import AccountService
from src.database_connector import DatabaseConnector
from src.product.product_repository import ProductRepository
from src.product.product_resource import ProductResource
from src.product.product_service import ProductService
from src.promotion.promotion_repository import PromotionRepository
from src.promotion.promotion_resource import PromotionResource
from src.promotion.promotion_service import PromotionService

app = Flask(__name__)
cors = CORS(app)

database_connector = DatabaseConnector()
product_repository = ProductRepository(database_connector)
product_service = ProductService(product_repository)
product_resource = ProductResource(product_service)
promotion_repository = PromotionRepository(database_connector)
promotion_service = PromotionService(promotion_repository)
promotion_resource = PromotionResource(promotion_service)
account_repository = AccountRepository(database_connector)
account_service = AccountService(account_repository)
account_resource = AccountResource(account_service)


@app.route('/products/<page>/<max_number_of_products_per_page>')
def get_products_page(page, max_number_of_products_per_page):
    return product_resource.get_products_page(request, page, max_number_of_products_per_page)


@app.route('/promotions')
def get_promotions():
    return promotion_resource.get_promotions(request)


@app.route('/login', methods=['POST'])
def login():
    pass


@app.route('/createAccount', methods=['POST'])
def create_account():
    if account_resource.validate_account_creation(request.get_json()):
        token = account_resource.create_account(request.get_json())
        return jsonify({'token': token})
    else:
        return jsonify({'error': 'Account already exists'})


@app.route('/signOut', methods=['POST'])
def sign_out():
    account_resource.sign_out(request.get_json().get('token'))
    return jsonify({'confirmation': 'successfully signed out'})


@app.route('/logIn', methods=['POST'])
def log_in():
    return account_resource.log_in(request.get_json())


@app.route('/addToCart', methods=['POST'])
def add_to_cart():
    return account_resource.add_to_cart(request.get_json())


@app.route('/cartProducts/<token>')
def get_all_products_in_cart(token):
    return account_resource.get_all_products_in_cart(token)


@app.route('/updateProductInCart', methods=['POST'])
def update_product_in_cart():
    return account_resource.update_product_in_cart(request.get_json())


@app.route('/deleteProductFromCart', methods=['POST'])
def delete_product_from_cart():
    return account_resource.delete_product_from_cart(request.get_json())


@app.route('/cartProducts/<token>/<product_id>')
def get_product_in_cart(token, product_id):
    return account_resource.get_product_in_cart(token, product_id)


@app.route('/orders', methods=['POST'])
def add_order():
    return account_resource.add_order(request.get_json())


@app.route('/orders/last/<token>')
def get_last_order(token):
    return account_resource.get_last_command(token)


@app.route('/orders/<token>')
def get_all_orders(token):
    return account_resource.get_all_commands(token)


@app.route('/productsInCommand/<command_id>')
def get_product_in_command(command_id):
    return account_resource.get_product_in_command_by_command_id(command_id)


if __name__ == '__main__':
    if os.environ.get('DEVENV') == 'prod':
        app.run(host="server")
    else:
        app.run(debug=True)
