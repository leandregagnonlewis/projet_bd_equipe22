# Projet du cours modèles et langages des bases de données
Ce projet est une application web en 3 couches. À la racine du projet vous trouverez donc plusieurs applications.
## Front-end
Pour compiler le front end vous devez avoir [nodejs](https://nodejs.org/en/)
et [angular](https://angular.io/guide/quickstart) d'installés sur votre machine.
Ensuite rendez vous a la racine du front-end et faites la commande `$npm install`  pour installer les dépendances.
Finalement, lancez la commande `$ng serve` pour déployer le projet localement.
L'IDE recommandée est VScode.

## Serveur Flask
Le serveur se compile dans pycharm. 
Les packages à instaler sont listés dans le ficher requirements.txt.

## Données 
Pour initialiser le container docker mysql, vous pouvez utliser la commande suivante à partir d'un PowerShell:
```
$ docker run -d `
$ -v <Path_vers_votre_repo>\projet_bd_equipe22\init_bd:/docker-entrypoint-initdb.d/:ro `
$ -p 1337:3306 `
$ --name ma_bd --rm `
$ --env="MYSQL_ALLOW_EMPTY_PASSWORD=true" mysql:8.0
```

Avant cela, il est important d'avoir permis le partage de votre disque dans docker.

