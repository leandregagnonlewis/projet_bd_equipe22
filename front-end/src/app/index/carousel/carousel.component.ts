import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { Promotion } from 'src/app/promotion';

@Component({
  selector: 'app-carousel',
  templateUrl: './carousel.component.html',
  styleUrls: ['./carousel.component.css']
})

export class CarouselComponent implements OnInit {
  promotions: Promotion[];
  currentlyDisplayedPromotion = 0;

  constructor(private apiService: ApiService) {
  }
   ngOnInit() {
     this.getPromotions();
   }

  getPromotions() {
    this.apiService.getPromotions().subscribe(
      (data: any) => { this.promotions = data.promotions; }
    );
  }

  getIndicatorClass(i) {
    if (i === this.currentlyDisplayedPromotion) {
      return 'active';
    }
    return '';
  }

  getCarouselClass(i) {
    if (i === this.currentlyDisplayedPromotion) {
      return 'item active';
    }
    return 'item';
  }

  decreaseCurrentlyDisplayedPromotion() {
    if (this.currentlyDisplayedPromotion !== 0) {
      this.currentlyDisplayedPromotion--;
    }
  }

  increaseCurrentlyDisplayedPromotion() {
    if (this.currentlyDisplayedPromotion !== this.promotions.length - 1) {
      this.currentlyDisplayedPromotion++;
    }
  }
}
