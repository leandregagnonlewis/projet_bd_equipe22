import { Component } from '@angular/core';
import {environment} from 'src/environments/environment';
import { MessageService } from './message.service';
import { Subscription } from 'rxjs';
import { ApiService } from './api.service';
import { Router } from '@angular/router';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  categories: Array<string> = environment.categories;
  errorMessage: String;
  loggedIn: boolean;
  errorExists = false;
  messageSubscription: Subscription;
  logInSubscription: Subscription;

  constructor(private messageService: MessageService, private apiService: ApiService,
    private router: Router, private authService: AuthService) {
    this.messageSubscription = messageService.messageSource$.subscribe(
      message => {
        this.errorMessage = message;
        this.errorExists = true;
    });
    this.loggedIn = authService.isLoggedIn;
    this.logInSubscription = authService.logInStream$.subscribe(isLoggedIn => this.loggedIn = isLoggedIn);
  }


  signOut() {
    this.apiService.postSignOut();
    this.authService.logout();
    this.router.navigate(['/index']);
  }
}
