import { Injectable } from '@angular/core';
import { MessageService } from './message.service';


@Injectable()
export class SecurityService {

    constructor(private messageService: MessageService) {}

  checkForSqlInjection(input: string) {
    const regex: RegExp = /(=|<|>|\'|--|\/|\+|;|\*|!|{|}|drop table|drop stored|alter table|alter stored|sp_|xp_|exec |execute |fetch|select|kill|selectsys|sysobjects|syscolumns|isnull|coalesce|dbo|tbl|usp)/gm;
    if (regex.test(input)) {
      this.messageService.sendMessage('Don\'t try this here boy!');
      return false;
    }
    return true;
  }
}
