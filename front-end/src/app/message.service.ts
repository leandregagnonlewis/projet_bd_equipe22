import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';


@Injectable()
export class MessageService {

  // Observable string sources
  private messageSource = new Subject<string>();

  // Observable string streams
  messageSource$ = this.messageSource.asObservable();


  // Service message commands
  sendMessage(message: string) {
    this.messageSource.next(message);
  }
}
