export enum Category {
    Promotions = 'Promotions',
    Africa = 'Africa',
    America = 'America',
    AsiaPacific = 'Asia & Pacific',
    Blends = 'Blends',
    Decaf = 'Decaf',
    Espresso = 'Espresso',
    Filtre = 'Filtre'
}
