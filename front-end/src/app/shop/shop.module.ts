import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


import { ShopMainComponent } from './shop-main/shop-main.component';
import { ShopNavbarComponent} from './shop-navbar/shop-navbar.component';
import { NumberFormatPipe } from './shop-navbar/number-format.pipe';
import { ShopSidebarComponent } from './shop-sidebar/shop-sidebar.component';
import { ItemDetailComponent } from './shop-main/item-detail/item-detail.component';


@NgModule({
    declarations: [
        ShopMainComponent,
        ShopNavbarComponent,
        NumberFormatPipe,
        ShopSidebarComponent,
        ItemDetailComponent
    ],
    imports: [
        CommonModule,
        FormsModule
    ],
    providers: [],
    entryComponents: [ItemDetailComponent]
  })
  export class ShopModule { }
