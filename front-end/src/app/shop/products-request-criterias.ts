export interface ProductRequestCriterias {
    category: string;
    search: string;
    roasts: string[];
    roasters: string[];
    sort: string;
    promotion: string;
}
