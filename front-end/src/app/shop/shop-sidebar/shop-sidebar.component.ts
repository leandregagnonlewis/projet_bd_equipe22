import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { map} from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ProductRequestCriterias } from '../products-request-criterias';
import {environment} from 'src/environments/environment';

@Component({
  selector: 'app-shop-sidebar',
  templateUrl: './shop-sidebar.component.html',
  styleUrls: ['./shop-sidebar.component.css']
})

export class ShopSidebarComponent implements OnInit {
  category$: Observable<string>;
  promotion$: Observable<string>;
  categories: string[] = environment.categories;
  roasts: Array<any> = [];
  roasters: Array<any> = [];
  requestCriterias: ProductRequestCriterias;
  categoryMenuIsVisible: boolean;
  roastMenuIsVisible: boolean;
  roasterMenuIsVisible: boolean;
  selectedRoasts: string[];
  selectedRoasters: string[];

  constructor(
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.route.paramMap.pipe(
      map ((params: ParamMap) => params.get('category'))).subscribe(x => {
        this.requestCriterias = {
          category: x, search: undefined, roasts: undefined, roasters: undefined, sort: undefined, promotion: undefined};
      }
    );
    this.route.paramMap.pipe(
      map ((params: ParamMap) => params.get('promotion'))).subscribe(x => {
        this.requestCriterias = {category: this.requestCriterias.category,
          search: this.requestCriterias.search,
          roasts: this.requestCriterias.roasts,
          roasters: this.selectedRoasters,
          sort: undefined,
          promotion: x };
      }
    );
  }



  toggleCategoryMenu() {
    this.categoryMenuIsVisible  = !this.categoryMenuIsVisible;
  }

  toggleRoastMenu () {
    this.roastMenuIsVisible = !this.roastMenuIsVisible;
  }

  toggleRoasterMenu () {
    this.roasterMenuIsVisible = !this.roasterMenuIsVisible;
  }

  menuItemClass(bool: boolean) {
    if (bool) {
      return 'custom-selected';
    }
  }

  changeCategory(newCategory) {
    this.resetRoastAndRoasters();
    this.requestCriterias = {category: newCategory,
      search: undefined,
      roasts: undefined,
      roasters: undefined,
      sort: undefined,
      promotion: undefined};
  }

  getAllProducts() {
    this.resetRoastAndRoasters();
    this.requestCriterias = {category: undefined,
      search: undefined,
      roasts: undefined,
      roasters: undefined,
      sort: undefined,
      promotion: undefined};
  }

  toggleRoast (roast: string) {
    if (!this.selectedRoasts) {this.selectedRoasts = []; }
    const roastIndex = this.selectedRoasts.indexOf(roast);
    if (roastIndex !== -1) {
      this.selectedRoasts.splice(roastIndex, 1);
    } else {
      this.selectedRoasts.push(roast);
    }
    if (this.selectedRoasts.length === 0) {
      this.selectedRoasts = undefined;
    }
    this.requestCriterias = {category: this.requestCriterias.category,
                            search: this.requestCriterias.search,
                            roasts: this.selectedRoasts,
                            roasters: this.requestCriterias.roasters,
                            sort: undefined,
                            promotion: this.requestCriterias.promotion};
  }

  toggleRoaster (roaster: string) {
    if (!this.selectedRoasters) {this.selectedRoasters = []; }
    const roasterIndex = this.selectedRoasters.indexOf(roaster);
    if (roasterIndex !== -1) {
      this.selectedRoasters.splice(roasterIndex, 1);
    } else {
      this.selectedRoasters.push(roaster);
    }
    if (this.selectedRoasters.length === 0) {
      this.selectedRoasters = undefined;
    }
    this.requestCriterias = {category: this.requestCriterias.category,
                            search: this.requestCriterias.search,
                            roasts: this.requestCriterias.roasts,
                            roasters: this.selectedRoasters,
                            sort: undefined,
                            promotion: this.requestCriterias.promotion};
  }

  wrapRoasts(items: Array<string>) {
    const copy = items.map((e) => {
      let temp = false;
      for (const i of this.roasts) {
        if (i.name === e) {
          temp = i.value;
        }
      }
      return {name: e, value: temp};
    });

    this.roasts = copy;
  }
  
  wrapRoasters(items: Array<string>) {
    const copy = items.map((e) => {
      let temp = false;
      for (const i of this.roasters) {
        if (i.name === e) {
          temp = i.value;
        }
      }
      return {name: e, value: temp};
    });

    this.roasters = copy;
  }
  resetRoastAndRoasters() {
    this.selectedRoasters = [];
    this.selectedRoasts = [];
    for (const item of this.roasters) {
      item.value = false;
    }
    for (const item of this.roasts) {
      item.value = false;
    }
  }
}
