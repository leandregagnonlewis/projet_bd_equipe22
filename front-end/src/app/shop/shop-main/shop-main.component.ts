import { Component, Input } from '@angular/core';
import { Product } from '../../product';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ItemDetailComponent } from './item-detail/item-detail.component';

@Component({
  selector: 'app-shop-main',
  templateUrl: './shop-main.component.html',
  styleUrls: ['./shop-main.component.css']
})

export class ShopMainComponent {

    @Input() products: Product[];

    constructor(private modalService: NgbModal) {}

    oneDArrayToTwoD(oldArray) {
      const newArray = Array();
      let tempArray = Array();
      let i = 1;
      for (const item of oldArray) {
        tempArray.push(item);
        if (i % 4 === 0) {
          newArray.push(tempArray);
          tempArray = Array();
        }
        i++;
      }
      if (tempArray.length >= 1) {
        newArray.push(tempArray);
      }
      return newArray;
    }

    openDetail(product: Product) {
      const modalRef = this.modalService.open(ItemDetailComponent, { size: 'lg' });
      modalRef.componentInstance.product = product;
    }

    togglePanelClass(product: Product, baseClass: string) {
      if (product.promotion !== null) {
        return baseClass + ' promo';
      } else { return baseClass; }
    }
}
