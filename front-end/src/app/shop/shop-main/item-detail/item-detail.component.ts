import { Input, Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Product } from '../../../product';
import { ApiService } from 'src/app/api.service';
import { AuthService } from 'src/app/auth.service';
import { MessageService } from 'src/app/message.service';

@Component({
    selector: 'app-item-detail',
    templateUrl: './item-detail.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./item-detail.component.css'],
  })
export class ItemDetailComponent implements OnInit {
    @Input() product: Product;
    alreadyInCart = false;
    quantity = 1;
    total = 0;
    errorExists = false;
    errorMessage: string;
    constructor(public activeModal: NgbActiveModal, private apiService: ApiService, private authService: AuthService,
      private messageService: MessageService) {}

    ngOnInit() {
      this.apiService.getProductFromCart(this.product.id).subscribe(
        (reponse: any) => {
           if (reponse.product !== null) {
             this.quantity = reponse.product.quantity;
             this.alreadyInCart = true;
           }
        }
      );
      this.calcTotal();
    }

    calcTotal() {
      this.total = this.quantity * this.product.price;
    }
    checkQuantity() {
      if (this.quantity === 0) {
        this.errorExists = true;
        this.errorMessage = 'You must specify a quantity for this product';
      } else if (!this.authService.isLoggedIn) {
        this.errorExists = true;
        this.errorMessage = 'You must be logged in to add product to your cart';
      } else {
        this.addProduct()
      }
    }
    addProduct() {
      this.apiService.postProductToCart(this.product.id, this.quantity).subscribe(
        (reponse: any) => { if (reponse.error) {
          this.errorExists = true;
          this.errorMessage = reponse.error;
        } else {
          this.activeModal.close(); }
        }
      );
    }
    updateProduct() {
      if (this.quantity === 0) {
        this.apiService.postDeleteProductFromCart(this.product.id).subscribe(
          (reponse: any) => { if (reponse.error) {
            this.errorExists = true;
            this.errorMessage = reponse.error;
          } else {
            this.activeModal.close(); }
          }
        );
      } else {
        this.apiService.postUpdateProductInCart(this.product.id, this.quantity).subscribe(
          (reponse: any) => { if (reponse.error) {
            this.errorExists = true;
            this.errorMessage = reponse.error;
          } else {
            this.activeModal.close(); }
          }
        );
      }
    }
  }
