import { Component, Input, OnChanges, EventEmitter, Output } from '@angular/core';
import { ProductRequestCriterias } from '../products-request-criterias';
import { Product } from '../../product';
import { ApiService } from '../../api.service';

@Component({
  selector: 'app-shop-navbar',
  templateUrl: './shop-navbar.component.html',
  styleUrls: ['./shop-navbar.component.css']
})

export class ShopNavbarComponent implements OnChanges {

  @Input() requestCriterias: ProductRequestCriterias;
  search = '';
  products: Product[];
  @Output() roasts = new EventEmitter<string[]>();
  @Output() roasters = new EventEmitter<string[]>();
  @Output() newSearch = new EventEmitter<boolean>();
  sortTypes: string[] = ['Name: A - Z', 'Name: Z - A', 'Price: Low to High', 'Price: High to Low'];
  firstItemOnPageIndex = 1;
  lastItemOnPageIndex = 10;
  numberOfItems = 10;
  firstPage = 1;
  currentPage = 1;
  lastPage = 1;
  maxNumberofItemsPerPage = 16;

  constructor(private apiService: ApiService) {
  }

  ngOnChanges() {
    this.currentPage = 1;
    this.getProducts();
  }

  getProducts() {
    this.apiService.getProducts(this.currentPage, this.maxNumberofItemsPerPage, this.requestCriterias).subscribe(
      (data: any) => { this.products = data.products;
                      this.roasts.emit(data.roasts);
                      this.roasters.emit(data.roasters);
                      this.numberOfItems = data.totalNumberOfProducts;
                      this.lastPage = data.numberOfPages;
                      this.firstItemOnPageIndex = data.firstProductIndex;
                      this.lastItemOnPageIndex = data.lastProductIndex; }
    );
  }

  cutPageNumbersLeft() {
    if (this.currentPage - this.firstPage > 5) {
      return true;
    }
    return false;
  }

  cutPageNumbersRight() {
    if (this.lastPage - this.currentPage > 5) {
      return true;
    }
    return false;
  }

  enumerateLowerPageNumbers() {
    let i = this.currentPage - 1;
    const pages: Array<Number> = new Array();
    const maxPages = 3 >= (8 - (this.lastPage - this.currentPage)) ? 3 : 8 - (this.lastPage - this.currentPage);
    while (i > this.firstPage && this.currentPage - i <= maxPages) {
      pages.push(i);
      i --;
    }
    return pages.sort((a: number, b: number) => a - b);
  }

  enumerateHigherPageNumbers() {
    let i = this.currentPage + 1;
    const pages: Array<Number> = new Array();
    const maxPages = 3 >= (8 - (this.currentPage - this.firstPage)) ? 3 : 8 - (this.currentPage - this.firstPage);
    while (i < this.lastPage && i - this.currentPage <= maxPages) {
      pages.push(i);
      i ++;
    }
    return pages.sort((a: number, b: number) => a - b);
  }

  toggleLeftArrowClass() {
    if (this.currentPage !== this.firstPage) {
      return '';
    }
    return 'custom-disabled';
  }
  toggleRightArrowClass() {
    if (this.currentPage !== this.lastPage) {
      return '';
    }
    return 'custom-disabled';
  }

  nextPage() {
    if (this.currentPage !== this.lastPage) {
      this.currentPage ++;
      this.getProducts();
    }
  }

  prevPage() {
    if (this.currentPage !== this.firstPage) {
      this.currentPage --;
      this.getProducts();
    }
  }

  changePage(pageNumber: number) {
    this.currentPage = pageNumber;
    this.getProducts();
  }

  changeMaxNumberOfItemsPerPage(max: number) {
    if (this.maxNumberofItemsPerPage !== max) {
      this.maxNumberofItemsPerPage = max;
    }
    this.currentPage = 1;
    this.getProducts();
  }

  toggleMaxNumbeOfItemsPerPageClass(max: number) {
    if (this.maxNumberofItemsPerPage === max) {
      return 'custom-disabled';
    }
  }

  launchSearch() {
    this.requestCriterias = {category: undefined,
      search: this.search,
      roasts: undefined,
      roasters: undefined,
      sort: undefined,
      promotion: undefined};
    this.search = '';
    this.newSearch.emit(true);
    this.getProducts();
  }

  sortProducts(sortType: string) {
    this.requestCriterias =  {category: this.requestCriterias.category,
                              search: this.requestCriterias.search,
                              roasts: this.requestCriterias.roasts,
                              roasters: this.requestCriterias.roasters,
                              sort: sortType,
                              promotion: this.requestCriterias.promotion};
    this.currentPage = 1;
    this.getProducts();
  }
}
