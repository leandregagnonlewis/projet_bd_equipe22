import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'numberFormat'})
export class NumberFormatPipe implements PipeTransform {
  transform(value: number): string {
    if (value < 10) {
        return value.toString() + '  ';
    }
    return value.toString();
  }
}
