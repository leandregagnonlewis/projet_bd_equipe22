import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';

import { AppComponent } from './app.component';
import { IndexComponent } from './index/index.component';
import { CarouselComponent } from './index/carousel/carousel.component';
import { ShopModule } from './shop/shop.module';
import { SignupComponent } from './account/signup/signup.component';
import { LoginComponent } from './account/login/login.component';
import { ApiService } from './api.service';
import { MessageService} from './message.service';

import { AccountModule } from './account/account.module';

import { CartModule} from './cart/cart.module';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth-guard';
import { OrderService } from './cart/order.service';
import { AuthGuardCheckout } from './auth-guard-checkout';
import { SecurityService } from './security.service';


@NgModule({
  declarations: [
    AppComponent,
    IndexComponent,
    CarouselComponent,
    LoginComponent,
    SignupComponent
  ],
  imports: [
    FormsModule,
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    ShopModule,
    AccountModule,
    CartModule
  ],
  providers: [
    ApiService,
    MessageService,
    CookieService,
    AuthService,
    AuthGuard,
    OrderService,
    AuthGuardCheckout,
    SecurityService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
