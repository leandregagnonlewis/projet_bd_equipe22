import { Promotion } from './promotion';

export class Product {
    id: number;
    name: string;
    category: string;
    origin: string;
    species: string;
    roast: string;
    roaster: string;
    price: number;
    flavour: string;
    imageUrl: string;
    promotion: Promotion;
  }
