import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class AuthService {
  isLoggedIn = false;
  private logInSource = new Subject<boolean>();
  logInStream$ = this.logInSource.asObservable();

  constructor(private cookieService: CookieService) {
    this.isLoggedIn = this.cookieService.check('FairBeans');
    this.logInSource.next(this.isLoggedIn);
  }

  login(token: string): void {
    this.logInSource.next(true);
    this.isLoggedIn = true;
    this.cookieService.set('FairBeans', token);
  }

  logout(): void {
    this.logInSource.next(false);
    this.isLoggedIn = false;
    this.cookieService.delete('FairBeans');
  }
}
