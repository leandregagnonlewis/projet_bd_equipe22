import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

import { AuthService } from './auth.service';
import { OrderService } from './cart/order.service';

@Injectable()
export class AuthGuardCheckout implements CanActivate {
  constructor(private authService: AuthService, private router: Router, private orderService: OrderService) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): boolean {

    return this.checkLogin() && this.checkOrderCompletion(next);
  }

  checkLogin(): boolean {
    if (this.authService.isLoggedIn) { return true; }
    this.router.navigate(['/index']);
    return false;
  }
  checkOrderCompletion(next: ActivatedRouteSnapshot): boolean {
    if (next.url[0].path === 'cart-address' && this.orderService.products !== null) {
      return true;
    }
    if (next.url[0].path === 'cart-payment' && this.orderService.address !== null) {
      return true;
    }
    if (next.url[0].path === 'cart-confirm' && this.orderService.paymentOption !== null) {
      return true;
    }
    if (next.url[0].path === 'cart-review' && this.orderService.confirmation) {
      return true;
    }
    this.router.navigate(['/cart']);
    return false;
  }
}
