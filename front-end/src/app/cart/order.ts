export class Order{
  shippingAddressId : number;
  billingAddressId : number;
  itemsOrdered: Array<{itemId: number, quantity: number}>
}
