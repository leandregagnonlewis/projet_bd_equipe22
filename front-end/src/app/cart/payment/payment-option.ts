export class PaymentOption {
    type: string;
    name: string;
    cardNumber: string;
    expiryDate: string;
    cvc: string;
}
