import {Component} from '@angular/core';
import { Router } from '@angular/router';
import { PaymentOption } from './payment-option';
import { OrderService } from '../order.service';
import { MessageService } from 'src/app/message.service';
import { SecurityService } from 'src/app/security.service';

@Component({
  selector: 'app-shipping-payment',
  templateUrl: './select-payment-method.component.html',
  styleUrls: ['./select-payment-method.component.css', '../cart-style.css']
})

export class SelectPaymentMethodComponent {
  paymentOption: PaymentOption = {
    type: '',
    name: '',
    cardNumber: '',
    expiryDate: '',
    cvc: ''
  };

  constructor(private router: Router, private orderService: OrderService,
    private messageService: MessageService, private securityService: SecurityService) {}

   paymentOptions = [{
     name: 'Mastercard',
     image: ['https://i.imgur.com/aYbB5fn.png'],
    },
    {
       name: 'Visa',
       image: ['https://i.imgur.com/kKCi1q3.png'],
    },
    {
       name: 'American Express',
       image: ['https://i.imgur.com/MTHQuhE.png'],
    }
  ];

  checked(option: string) {
    this.paymentOption.type = option;
  }

  validate() {
    if (this.checkName() && this.checkNumber() && this.checkExpiry() && this.checkCvc() && this.checkType()) {
      this.orderService.setPaymentOption(this.paymentOption);
      this.continue();
    }
  }

  checkType(): boolean {
    if (this.paymentOption.type === '') {
      this.messageService.sendMessage('You must select a payment type...');
      return false;
    }
    return true;
  }

  checkName(): boolean {
    const regex: RegExp = /[^A-Za-zàâçéèêëîïôûùüÿñæœ. -]/;
    if (regex.test(this.paymentOption.name) || this.paymentOption.name === '') {
      this.messageService.sendMessage('This is not a valid name...');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.paymentOption.name);
  }

  checkNumber(): boolean {
    const regex = /^[0-9]{16}$/;
    if (!regex.test(this.paymentOption.cardNumber)) {
      this.messageService.sendMessage('This is not a valid card number...');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.paymentOption.cardNumber);
  }

  checkExpiry(): boolean {
    const regex = /^[0-9]{2}\/[0-9]{2}$/;
    if (!regex.test(this.paymentOption.expiryDate)) {
      this.messageService.sendMessage('This is not a valid expiry date. Format is "MM/AA"');
      return false;
    }
    return true;
  }

  checkCvc(): boolean {
    const regex = /^[0-9]{3}/;
    if (!regex.test(this.paymentOption.cvc)) {
      this.messageService.sendMessage('This is not a valid cvc...');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.paymentOption.cvc);
  }

  continue() {
    this.router.navigate(['/cart-confirm']);
  }
}
