import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { MessageService } from 'src/app/message.service';
import { routerNgProbeToken } from '@angular/router/src/router_module';
import { Product } from 'src/app/product';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.css', '../cart-style.css']
})


export class ShoppingCartComponent implements OnInit {
  subtotal = 0.00;
  items: Array<Product>;


  constructor(private router: Router, private apiService: ApiService, private messageService: MessageService,
    private orderService: OrderService) {}

  ngOnInit() {
    this.apiService.getProductsFromCart().subscribe(
      (data: any) => { this.items = data.products; }
    );
  }
  getSumItems(itemsInCart) {
    return itemsInCart.length;
  }

  addQuantity(item: any, index: number) {
    if (item.quantity < 10) {
      this.apiService.postUpdateProductInCart(item.id, item.quantity + 0.5).subscribe(
        (data: any) => item = this.items[index] = data.updatedProduct
      );
    }
  }

  substractQuantity(item, index: number) {
    if (item.quantity > 0.5) {
      this.apiService.postUpdateProductInCart(item.id, item.quantity - 0.5).subscribe(
        (data: any) => {
          item = this.items[index] = data.updatedProduct;
        } 
      );
    }
  }


  calcTotalItem(itemsInCart) {
    let subtotal = 0;
    for (let i = 0; i < itemsInCart.length; i++) {
      let itemPrice = itemsInCart[i].price * itemsInCart[i].quantity;
      if (itemsInCart[i].promotion !== null) {itemPrice -= itemPrice * itemsInCart[i].promotion.discount * 0.01; }
      subtotal += itemPrice;
    }
    this.subtotal = subtotal;
  }

  deleteItem(id: number, index: number) {
    this.items.splice(index, 1);
    this.apiService.postDeleteProductFromCart(id).subscribe(
      (reponse: any) => { if (reponse.error) {
        this.messageService.sendMessage(reponse.errorMessage);
      }
    });
  }

  checkout() {
    this.orderService.setProducts(this.items);
    this.orderService.setTotal(this.subtotal);
    this.router.navigate(['/cart-address']);
  }
}
