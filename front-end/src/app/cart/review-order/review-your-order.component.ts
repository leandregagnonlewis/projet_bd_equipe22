import { Component, OnInit } from '@angular/core';
import { OrderService } from '../order.service';
import { Address } from '../address/address';
import { ApiService } from 'src/app/api.service';

@Component({
  selector: 'app-cart-review',
  templateUrl: './review-your-order.component.html',
  styleUrls: ['./review-your-order.component.css', '../cart-style.css']
})

export class ReviewYourOrderComponent implements OnInit {
  address: Address = null;
  id: number = null;
  numberOfItems: number = null;

  constructor(private orderService: OrderService, private apiService: ApiService) {}
  ngOnInit() {
    this.address = this.orderService.address;
    this.numberOfItems = this.orderService.products.length;
    this.orderService.reset();
  }

  getNumberItemsMessage() {
    const message = ' will be delivered to:';
    if (this.numberOfItems === 1) {
      return '1 item ' + message;
    } else {
      return this.numberOfItems.toString() + ' items ' + message;
    }
  }
}



