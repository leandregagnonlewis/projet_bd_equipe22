import { NgModule } from '@angular/core';

import { Routes, RouterModule } from '@angular/router';
import { SelectShippingAddressComponent } from './address/select-shipping-address.component';
import { SelectPaymentMethodComponent } from './payment/select-payment-method.component';
import { ConfirmOrderComponent } from './confirm-order/shipping-confirm-order.component';
import { ReviewYourOrderComponent } from './review-order/review-your-order.component';
import { AccountModule } from '../account/account.module';

import { ShoppingCartComponent} from './shopping/shopping-cart.component';
import { ShippingConfirmationItemsComponent} from './confirm-order/shipping-confirmation-items.component';
import { AuthGuardCheckout } from '../auth-guard-checkout';

const routes: Routes = [
  { path: 'cart-address', component: SelectShippingAddressComponent,  canActivate: [AuthGuardCheckout]},
  { path: 'cart-payment', component: SelectPaymentMethodComponent, canActivate: [AuthGuardCheckout]},
  { path: 'cart-confirm', component: ConfirmOrderComponent,  canActivate: [AuthGuardCheckout]},
  { path: 'cart-review', component: ReviewYourOrderComponent,  canActivate: [AuthGuardCheckout]},
];


@NgModule({
  imports: [RouterModule.forChild(routes), AccountModule],
  exports: [RouterModule]
})
export class CartRoutingModule { }
