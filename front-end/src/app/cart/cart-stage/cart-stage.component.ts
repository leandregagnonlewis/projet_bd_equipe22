import {Component, Input} from '@angular/core';

@Component({
  selector: 'app-cart-stage',
  templateUrl: './cart-stage.component.html',
  styleUrls: ['./cart-stage.component.css']
})

export class CartStageComponent {
  @Input() order_step;

  steps = [{
    step: 1,
    title: 'Shopping',
    route: '/cart-basket'
  }, {
    step: 2,
    title: 'Address',
    route: '/cart-address'
  }, {step: 3,
    title: 'Payment',
    route: '/cart-payment'},
    {
      step: 4,
      title: 'Confirm Order',
      route: '/cart-confirm'
    }];
}
