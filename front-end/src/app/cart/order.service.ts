import { Injectable } from '@angular/core';
import { Address } from './address/address';
import { PaymentOption } from './payment/payment-option';
import { Product } from '../product';
import { ApiService } from '../api.service';
import { MessageService } from '../message.service';


@Injectable()
export class OrderService {
  constructor(private apiService: ApiService, private messageService: MessageService) {}

  products: Array<Product> = null;
  orderTotal = null;
  address: Address = null;
  paymentOption: PaymentOption = null;
  confirmation = false;

  setProducts(products: Array<Product>) {
    this.products = products;
  }

  setAddress(address: Address) {
    this.address = address;
  }

  setPaymentOption(paymentOption: PaymentOption) {
    this.paymentOption = paymentOption;
  }

  setConfirmation(value: boolean) {
    this.confirmation = value;
  }

  setTotal(total: number) {
    this.orderTotal = total;
  }

  placeOrder() {
    this.confirmation = true;
    this.apiService.postOrder(this.address).subscribe(
      (reponse: any) => { if (reponse.error) {
        this.messageService.sendMessage(reponse.error);
      }}
    );
  }

  reset() {
    this.products = null;
    this.orderTotal = null;
    this.address = null;
    this.paymentOption = null;
    this.confirmation = false;
  }
}
