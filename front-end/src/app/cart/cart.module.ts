import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CartRoutingModule } from './cart-routing.module';
import { CartStageComponent } from './cart-stage/cart-stage.component';
import { SelectShippingAddressComponent} from './address/select-shipping-address.component';
import { ShoppingCartComponent } from './shopping/shopping-cart.component';
import { ConfirmOrderComponent } from './confirm-order/shipping-confirm-order.component';
import { ShippingConfirmationItemsComponent } from './confirm-order/shipping-confirmation-items.component';
import { SelectPaymentMethodComponent } from './payment/select-payment-method.component';
import { ReviewYourOrderComponent } from './review-order/review-your-order.component';
import { AccountModule } from '../account/account.module';

@NgModule({
  declarations: [
    CartStageComponent,
    SelectShippingAddressComponent,
    ShoppingCartComponent,
    ConfirmOrderComponent,
    SelectPaymentMethodComponent,
    ReviewYourOrderComponent,
    ShippingConfirmationItemsComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    CartRoutingModule,
    AccountModule
  ],
  entryComponents: [],
  providers: [],
})


export class CartModule {}




