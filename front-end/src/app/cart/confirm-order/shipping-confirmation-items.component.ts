import { Component } from '@angular/core';
import { OrderService } from '../order.service';

@Component({
  selector: 'app-shipping-confirmation-items',
  templateUrl: './shipping-confirmation-items.component.html',
  styleUrls: ['./shipping-confirmation-items.component.css', '../cart-style.css']
})

export class ShippingConfirmationItemsComponent {
  constructor(private orderService: OrderService) {}
  getTotal(item: any) {
    let price: number = item.price * item.quantity;
    if (item.promotion !== null) {
      price -= item.promotion.discount * .01 * price;
    }
    return (price).toFixed(2);
  }
}
