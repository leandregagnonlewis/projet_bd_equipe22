import { Component } from '@angular/core';
import { OrderService } from '../order.service';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-shipping-confirmation',
  templateUrl: './shipping-confirm-order.component.html',
  styleUrls: ['./shipping-confirm-order.component.css', '../cart-style.css']
})

export class ConfirmOrderComponent {
  constructor(private orderService: OrderService, private router: Router) {}

  placeOrder() {
    this.orderService.placeOrder();
    this.router.navigate(['cart-review']);
  }
}
