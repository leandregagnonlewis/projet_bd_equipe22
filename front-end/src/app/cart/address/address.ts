export class Address {
    firstName: string;
    lastName: string;
    street: string;
    city: string;
    zipcode: string;
    country: string;
}