import {Component, Input} from '@angular/core';
import {Router} from "@angular/router";
import { OrderService } from '../order.service';
import { MessageService } from 'src/app/message.service';
import { SecurityService } from 'src/app/security.service';
import { Address } from 'src/app/cart/address/address';
@Component({
  selector: 'app-shipping-address',
  templateUrl: './select-shipping-address.component.html',
  styleUrls: ['./select-shipping-address.component.css', '../cart-style.css']
})

export class SelectShippingAddressComponent {
  address: Address = {
    firstName: '',
    lastName: '',
    street: '',
    city: '',
    zipcode: '',
    country: ''
  };

  constructor(private _router: Router, private orderService: OrderService, private messageService: MessageService,
    private securityService: SecurityService) {}

  submit() {
    if (this.checkNames() && this.checkAddress() && this.checkCityAndCountry() && this.checkZipcode()) {
      this.orderService.setAddress(this.address);
      this.continue();
    }
  }
  continue() {
    this._router.navigate(['/cart-payment']);
  }

  checkNames() {
    const regex: RegExp = /[^A-Za-zàâçéèêëîïôûùüÿñæœ.-]/;
    if (regex.test(this.address.firstName) || regex.test(this.address.lastName) || this.address.firstName === '' ||
    this.address.lastName === '') {
      this.messageService.sendMessage('Come on, give us your real name. We\'ll keep it a secret');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.address.firstName) &&
    this.securityService.checkForSqlInjection(this.address.lastName);
  }

  checkAddress() {
    const regex = /^[0-9]{1,5}[A-Za-z\- ]{3,}[\w]{0,}$/;
    if (!regex.test(this.address.street)) {
      this.messageService.sendMessage('Come on, give us your real address. We\'ll keep it a secret');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.address.street);
  }

  checkCityAndCountry() {
    const regex: RegExp = /[^A-Za-zàâçéèêëîïôûùüÿñæœ.-]/;
    if (regex.test(this.address.city) || regex.test(this.address.country)) {
      this.messageService.sendMessage('Come on, give us your real address. We\'ll keep it a secret');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.address.city) && this.securityService.checkForSqlInjection(this.address.country);
  }

  checkZipcode() {
    const regex = /^[A-Z][0-9][A-Z][ ][0-9][A-Z][0-9]$/;
    if (!regex.test(this.address.zipcode) && this.address.country === 'Canada') {
      this.messageService.sendMessage('Invalid zipcode. Format is X0X 0X0');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.address.zipcode);
  }
}
