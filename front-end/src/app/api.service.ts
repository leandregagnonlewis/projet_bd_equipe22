import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import {environment} from 'src/environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { Address } from './cart/address/address';

@Injectable()
export class ApiService {

  constructor(private http: HttpClient, private cookieService: CookieService) { }

  getProducts(page, maxNumberOfItemsPerPage, criterias) {
    let params = new HttpParams();
    Object.keys(criterias).forEach((item) => {
      if (criterias[item]) {
        params = params.set(item, criterias[item]); }
    });
    return this.http.get(`${environment.apiUrl}/products/${page}/${maxNumberOfItemsPerPage}`, {params});
  }

  getPromotions() {
    return this.http.get(`${environment.apiUrl}/promotions`);
  }

  postAccountCreation(accountInfo) {
    return this.http.post(`${environment.apiUrl}/createAccount`, accountInfo);
  }

  postSignOut() {
    return this.http.post(`${environment.apiUrl}/signOut`, {'token': this.cookieService.get('FairBeans')});
  }

  postLogIn(accountInfo)  {
    return this.http.post(`${environment.apiUrl}/logIn`, accountInfo);
  }

  postProductToCart(productId: number, quantity: number) {
    return this.http.post(`${environment.apiUrl}/addToCart`, {'token': this.cookieService.get('FairBeans'), productId, quantity});
  }

  getProductsFromCart() {
    return this.http.get(`${environment.apiUrl}/cartProducts/${this.cookieService.get('FairBeans')}`);
  }

  getProductFromCart(product_id: number) {
    return this.http.get(`${environment.apiUrl}/cartProducts/${this.cookieService.get('FairBeans')}/${product_id}`);
  }

  postUpdateProductInCart(productId: number, quantity: number) {
    return this.http.post(`${environment.apiUrl}/updateProductInCart`, {'token': this.cookieService.get('FairBeans'), productId, quantity});
  }

  postDeleteProductFromCart(productId: number) {
    return this.http.post(`${environment.apiUrl}/deleteProductFromCart`, {'token': this.cookieService.get('FairBeans'), productId});
  }

  postOrder(address: Address) {
    return this.http.post(`${environment.apiUrl}/orders`, {'token': this.cookieService.get('FairBeans'), 'address': address});
  }

  getLastOrder() {
    return this.http.get(`${environment.apiUrl}/orders/last/${this.cookieService.get('FairBeans')}`);
  }

  getAllOrders() {
    return this.http.get(`${environment.apiUrl}/orders/${this.cookieService.get('FairBeans')}`);
  }

  getProductsInCommand(id: number) {
    return this.http.get(`${environment.apiUrl}/productsInCommand/${id}`);
  }
}
