import { Component } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { EmailValidator } from '@angular/forms';
import { MessageService } from 'src/app/message.service';
import { post } from 'selenium-webdriver/http';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { SecurityService } from 'src/app/security.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['../../account/account-new-account.css']
})

export class SignupComponent {
  errorExists = false;
  email = '';
  password1 = '';
  password2 = '';
  firstName = 'f';
  lastName = '';

  constructor(private apiService: ApiService,
    private messageService: MessageService, private cookieService: CookieService, private router: Router,
    private authService: AuthService, private securityService: SecurityService) {}

  confirmForm() {
    if (this.testEmail() && this.testPassword()) {
      this.post();
    }
  }
  showErrorMessage(message: string) {
    this.messageService.sendMessage(message);
  }

  post() {
    this.apiService.postAccountCreation({'email': this.email, 'password': this.password1}).subscribe(
      (reponse: any) => { if (reponse.error) {
        this.showErrorMessage(reponse.error);
      } else {
        this.authService.login(reponse.token);
        this.router.navigate(['/index']);
                          } }
    );
  }

  testEmail() {
    const regexpEmail = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.+[a-z]{2,4}$');
    if (!regexpEmail.test(this.email)) {
      this.showErrorMessage('The email adress you entered is invalid');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.email);
  }
  testPassword() {
    const regexp1 = new RegExp('^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,14}$');
    if (!regexp1.test(this.password1)) {
      this.showErrorMessage('The password must be between 6 and 14 character and contain at least one number and one letter');
      return false;
    }
    if (this.password1 !== this.password2) {
      this.showErrorMessage('The passwords doesn\'t match');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.password1);
  }
}

