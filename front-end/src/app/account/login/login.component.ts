import { Component } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/api.service';
import { MessageService } from 'src/app/message.service';
import { AuthService } from 'src/app/auth.service';
import { SecurityService } from 'src/app/security.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../account-new-account.css']
})

export class LoginComponent {
  email = '';
  password = '';

  constructor(private apiService: ApiService,
    private messageService: MessageService, private cookieService: CookieService,
    private router: Router, private authService: AuthService, private securityService: SecurityService) {}

  confirmForm() {
    if (this.testEmail() && this.testPassword()) {
      this.post();
    }
  }

  showErrorMessage(message: string) {
    this.messageService.sendMessage(message);
  }

  post() {
    this.apiService.postLogIn({'email': this.email, 'password': this.password}).subscribe(
      (reponse: any) => { if (reponse.error) {
        this.showErrorMessage(reponse.error);
      } else {
        this.authService.login(reponse.token);
        this.router.navigate(['/index']);
                          } }
    );
  }
  testEmail() {
    const regexpEmail = new RegExp('^[a-z0-9._%+-]+@[a-z0-9.-]+\.+[a-z]{2,4}$');
    if (!regexpEmail.test(this.email)) {
      this.showErrorMessage('The email adress you entered is invalid');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.email);
  }
  testPassword() {
    const regexp1 = new RegExp('^(?=.*[0-9]+.*)(?=.*[a-zA-Z]+.*)[0-9a-zA-Z]{6,14}$');
    if (!regexp1.test(this.password)) {
      this.showErrorMessage('This password is invalid');
      return false;
    }
    return this.securityService.checkForSqlInjection(this.password);
  }
}


