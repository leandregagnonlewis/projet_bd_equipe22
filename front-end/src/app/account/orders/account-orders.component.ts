import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/api.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderDetailComponent } from './order-detail.ts/order-detail.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-account-orders',
  templateUrl: './account-orders.component.html',
  styleUrls: ['./account-orders.component.css']
})

export class AccountOrdersComponent implements OnInit {

  orders: Array<any> = [];

  constructor(private apiService: ApiService, private modalService: NgbModal, private router: Router) {}

  ngOnInit() {
    this.apiService.getAllOrders().subscribe(
      (data: any) => {
        this.orders = data.orders;
      }
    );
  }

  goToShop() {
    this.router.navigate(['/shop']);
  }

  showDetail(id: number) {
    const modalRef = this.modalService.open(OrderDetailComponent);
    modalRef.componentInstance.id = id;
  }

}
