import { Input, Component, ViewEncapsulation, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ApiService } from 'src/app/api.service';

@Component({
    selector: 'app-order-detail',
    templateUrl: './order-detail.component.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./order-detail.component.css'],
  })
export class OrderDetailComponent implements OnInit {
    @Input() id: number;
    products: Array<any>;

    constructor(public activeModal: NgbActiveModal, private apiService: ApiService) {}

    ngOnInit() {
      this.apiService.getProductsInCommand(this.id).subscribe(
        (reponse: any) => {
           this.products = reponse.products;
           }
      );
    }
  }
