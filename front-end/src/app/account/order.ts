export class Order {
  id: number;
  total: number;
  addressFacture: string;
  addressDelivery: string;
  date: Date;
}
