import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AccountOrdersComponent} from './orders/account-orders.component';
import { OrderDetailComponent } from './orders/order-detail.ts/order-detail.component';



@NgModule({
  declarations: [
    AccountOrdersComponent,
    OrderDetailComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  entryComponents: [OrderDetailComponent]
})


export class AccountModule {}




