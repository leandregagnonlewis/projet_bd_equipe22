export class Promotion {
id: number;
discount: number;
attribute: string;
selector: string;
description: string;
}
