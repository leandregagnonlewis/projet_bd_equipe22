import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ShoppingCartComponent} from './cart/shopping/shopping-cart.component';

import {IndexComponent} from './index/index.component';
import {ShopSidebarComponent} from './shop/shop-sidebar/shop-sidebar.component';
import {LoginComponent} from './account/login/login.component';
import {SignupComponent} from './account/signup/signup.component';

import { AuthGuard } from './auth-guard';
import { AccountOrdersComponent } from './account/orders/account-orders.component';


const routes: Routes = [
  { path: '', redirectTo: '/index', pathMatch: 'full' },
  { path: 'cart', component: ShoppingCartComponent, canActivate: [AuthGuard]},
  { path: 'index', component: IndexComponent},
  { path: 'shop', component: ShopSidebarComponent},
  { path: 'login', component: LoginComponent},
  { path: 'signup', component: SignupComponent},
  { path: 'orders', component: AccountOrdersComponent, canActivate: [AuthGuard]},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
